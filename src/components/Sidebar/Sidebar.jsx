import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { selectProductsTotalPrice } from '../../redux/cart/cart.selector';
import { hiddenSidebar } from '../../redux/cart/slice';

import './Sidebar.css';
import { MdOutlineClose } from "react-icons/md";
import Productitem from '../Productitem/Productitem';

import formatPrice from '../../utils/formatPrice';

const Sidebar = () => {
  const { products } = useSelector((rootReducer) => rootReducer.cartReducer);

  const dispach = useDispatch()

  const handleCloseSidebar = () => {
    dispach(hiddenSidebar(false))
  }

  const productsTotalPrice = useSelector(selectProductsTotalPrice)

  return (
    <div className="container_sidebar">
       <div className="header_sidebar" onClick={handleCloseSidebar}>
            <h2> Seu Carrinho </h2>
            <MdOutlineClose size={30}/>
       </div>
       
       <div className="content_cart">
            {products.map(product => (
              <Productitem product={product}/>
            ))}
       </div>

      <div className='content_total_price'>
        <h1> {productsTotalPrice === 0 ? "" : `Total: ${formatPrice(productsTotalPrice, "BRL")}`} </h1>
      </div>
    </div>
  )
};

export default Sidebar;
