import React from 'react';

import './Loading.css';
import LoadingIcon from '../../assets/loading.png'

const Loading = () => {
  return (
    <div className='loading_container'>
        <img className="loading_icon" src={LoadingIcon} alt="loading icon" />
    </div>
  );
};

export default Loading;
