import React from "react";

import './Header.css'

import Searchbar from "../Searchbar/Searchbar";
import Cartbutton from "../Cartbutton/Cartbutton";

const Header = () => {
    return (
        <header className="header">
            <div className="container">
                <Searchbar />
                <Cartbutton />
            </div>
        </header>
    )
}

export default Header;

