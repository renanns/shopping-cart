import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import Products from '../../api/products';

import './Content.css';

import Cardproduct from '../Cardproduct/Cardproduct';
import Loading from '../Loading/Loading';

const Content = () => {
  const [product, setProduct] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const {searchProducts} = useSelector(rootReducer => rootReducer.cartReducer);

  useEffect(() => {
    Products(searchProducts || 'iphone').then((resp) => {
      setProduct(resp)
      setTimeout(() => { 
        setIsLoading(false) 
      }, 1000)
    })
  }, [searchProducts]);


  return (
    <section className='content container'>
        { (isLoading && <Loading/>) || (<Cardproduct product={product}/>) }
    </section>
  );
};

export default Content;