import React from 'react';

import './Cartbutton.css';
import { MdOutlineShoppingCart } from "react-icons/md";

import { useSelector, useDispatch } from 'react-redux';
import { selectProductsCount } from '../../redux/cart/cart.selector';
import { hiddenSidebar } from '../../redux/cart/slice';

const Cartbutton = () => {
  const numberProductsAdd = useSelector(selectProductsCount);

  const dispach = useDispatch();
  
  const handleShowCart = () => {
    dispach(hiddenSidebar(true));
  }
 
  return (
    <button type="button" className="cart_button" onClick={handleShowCart}> 
        <MdOutlineShoppingCart /> 
        <span className='cart_status'>{numberProductsAdd}</span>
    </button>
  )
}

export default Cartbutton;
