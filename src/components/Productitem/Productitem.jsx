import "./Productitem.css";

import { MdOutlineClose } from "react-icons/md";

import { MdOutlineAdd } from "react-icons/md";
import { MdOutlineRemove } from "react-icons/md";

import formatPrice from "../../utils/formatPrice" 

import { useDispatch } from "react-redux";
import { decreaseProductQuantity, increaseProductQuantity, removeProductCart } from "../../redux/cart/slice";

const Productitem = ({product}) => {

  const dispach = useDispatch()

  const handleIncrementProduct = () => {
    dispach(increaseProductQuantity(product.id))
  };

  const handleDecrementProduct = () => {
    dispach(decreaseProductQuantity(product.id))
  };

  const handleDeleteProductCart = () => {
    dispach(removeProductCart(product.id))
  };


  return (
    <div className="container_product" key={product.id}>
        <div className="box_image">
            <img src={`https://http2.mlstatic.com/D_${product.thumbnail_id}-W.jpg`} alt="product item" />
        </div>

        <div className="box_desc">
          <h4>{product.title}</h4>

          <div className="container_quantity">
            <div onClick={handleDecrementProduct}>
              <MdOutlineRemove size={20}/>
            </div>
            
            <span>{product.quantity}</span>
            
            <div onClick={handleIncrementProduct}>
              <MdOutlineAdd size={20}/>
            </div>
          </div>
          
          <span>{formatPrice(product.price, "BRL")}</span>
        </div>

        <div onClick={handleDeleteProductCart}>
          <MdOutlineClose size={20} />
        </div>
    </div>
  )
};

export default Productitem;
