import React from 'react';
import { useState } from 'react';
import { MdOutlineSearch } from "react-icons/md";

import { useDispatch } from 'react-redux';

import './Searchbar.css';
import { searchProducts } from '../../redux/cart/slice';

const Searchbar = () => {
  const [searchValue, setSearchValue] = useState('');
  
  const dispach = useDispatch();
  
  const handleFilter = (event) => {
    event.preventDefault();
    dispach(searchProducts(searchValue));
  }

  return (
    <form className="search_form" onSubmit={handleFilter}>
        <input type="search"
               value={searchValue} 
               placeholder="search products" 
               className="search_input" 
               required
               onChange={({target}) => setSearchValue(target.value)} 
        />
        <button type="submit" className="search_button"> <MdOutlineSearch /> </button>
    </form>
  )
}

export default Searchbar;
