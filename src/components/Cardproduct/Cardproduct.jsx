import React from 'react';
import PropTypes from 'prop-types';

import './Cardproduct.css';

import formatPrice from '../../utils/formatPrice.js'

import { useDispatch } from 'react-redux';
import { addProductCart } from '../../redux/cart/slice.js';

const Cardproduct = ({product}) => {
  const dispach = useDispatch();
  
  const handleClickAddCart = (item) => {
    dispach(addProductCart(item))
  }

  return (
    <section className='product_card' key={product.id}>
        {product.map((item) => {
          return (
            <div key={item.id} className='card_item'>
                <img className="card_image" src={`https://http2.mlstatic.com/D_${item.thumbnail_id}-W.jpg`} alt="item product" />
                
                <div className="card_info">
                    <h2 className="card_price">
                      {formatPrice(item.price, 'BRL')}
                    </h2>
                    <h2 className="card_title">
                      {item.title}
                    </h2>
                </div>
    
                <button
                  aria-label='increment value'
                  type="button" 
                  className='card_button_add' 
                  onClick={() => handleClickAddCart(item)}
                  > Adicionar ao carrinho </button>
            </div>
          )
        })}
    </section>
  );
};

Cardproduct.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
  }))
};

export default Cardproduct;