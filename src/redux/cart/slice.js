import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    products: [],
    searchProducts: '',
    showCart: false
};

const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        addProductCart: (state, action) => {
            // Iterando sobre os products do redux para verificar se o produto atual tem o id igual ao do produto que está sendo dispachado e recebido no payload.
            const productIsAlreadyInCart = state.products.some(
                (product) => product.id === action.payload.id
            );

            // Se o produto já estiver no carrinho products recebe  uma iteração sobre cada product para verificar se o id do product atual é igual a do product do payload
            //se for verdade, é inserido é retornado o product atual aumentando a sua quantidade, caso contrário retona o product sem alterações
            if(productIsAlreadyInCart) {
                state.products = state.products.map((product) => 
                    product.id === action.payload.id
                    ? {...product, quantity: product.quantity + 1} 
                    : product
                );

                return;
            }

            // Se o product atual não estiver no carrinho, inserimos o product com a quantidade de 1
            state.products = [...state.products, { ...action.payload, quantity: 1 }]
        },

        removeProductCart: (state, action) => {
            state.products = state.products.filter(product => product.id !== action.payload);
        },

        increaseProductQuantity: (state, action) => {
            state.products = state.products.map((product) => 
                product.id === action.payload
                ? {...product, quantity: product.quantity + 1} 
                : product
            );  
        },

        decreaseProductQuantity: (state, action) => {
            state.products = state.products.map((product) => 
                product.id === action.payload
                ? {...product, quantity: product.quantity - 1} 
                : product
            )
            .filter(product => product.quantity > 0); 
        },

        searchProducts: (state, action) => {
            state.searchProducts = action.payload
        },

        hiddenSidebar: (state, action) => {
            state.showCart = action.payload
        }
    }
})

export const { addProductCart,
               removeProductCart, 
               increaseProductQuantity, 
               decreaseProductQuantity, 
               searchProducts, 
               hiddenSidebar } = cartSlice.actions;

export default cartSlice.reducer;