import React from "react";

import Header from "./components/Header/Header";
import Content from "./components/Content/Content";
import Sidebar from "./components/Sidebar/Sidebar";
import { useSelector } from "react-redux";

function App() {

  const {showCart} = useSelector(rootReducer => rootReducer.cartReducer)

  return (
    <div>
      <Header/>
      {showCart ? <Sidebar /> : ""}

      <Content />
    </div>
  );
};

export default App;
