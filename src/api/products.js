const Products = async (type) => {
    const response = await fetch(`https://api.mercadolibre.com/sites/MLB/search?q=${type}`);
    const data = await response.json()
    return data.results
}
export default Products;